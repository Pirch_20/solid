using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnManager : BaseManager
{
    public void StartRespawn(GameObject _gameObject, float _respawnTime = 5f)
    {
        StartCoroutine(IERespawn(_gameObject, _respawnTime));
    }
    
    public IEnumerator IERespawn(GameObject _gameObject, float _respawnTime = 5f)
    {
        yield return new WaitForSeconds(_respawnTime);
        _gameObject.SetActive(true);
    }
}
